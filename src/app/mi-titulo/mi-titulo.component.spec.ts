import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiTituloComponent } from './mi-titulo.component';

describe('MiTituloComponent', () => {
  let component: MiTituloComponent;
  let fixture: ComponentFixture<MiTituloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiTituloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiTituloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
