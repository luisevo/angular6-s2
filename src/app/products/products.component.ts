import {Component, OnInit, OnChanges, OnDestroy, DoCheck, Input, SimpleChange, SimpleChanges, Output, EventEmitter} from '@angular/core';
import {Product} from '../models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnChanges, OnDestroy, DoCheck {
  @Input() products: Product[];
  @Output() addCart = new EventEmitter();

  constructor() { }
  ngOnInit() {
    console.log('ngOnInit');
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges', changes);
  }
  ngOnDestroy() {
    console.log('ngOnDestroy');
  }
  ngDoCheck() {
    console.log('ngDoCheck', this.products);
  }

  addCartHanlder(product) {
    this.addCart.emit(product);
  }

}
