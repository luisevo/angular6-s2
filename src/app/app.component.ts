import {Component, OnInit} from '@angular/core';
import {Product} from './models/product.model';
import {LocalService} from './local.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular6-s02';
  urls = [
    'Inicio',
    'Nosotros',
  ];
  urlActive = 'Inicio';
  products: Product[] = [];

  cart: any[] = [];

  constructor(private localService: LocalService) {
    this.localService.getProducts()
      .subscribe(result => this.products = result);
  }

  urlSelected(event) {
    this.urlActive = event;
  }

  addProductCart(product) {
    this.cart.push(product);
  }
}
