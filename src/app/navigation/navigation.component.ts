import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Input() urls: string[];
  @Output() urlChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  clickHandler(url: string) {
    this.urlChange.emit(url);
  }

}
